﻿
create function meta.current_release_id()
returns int
begin
	declare @solution_id int = meta.current_solution_id();
	declare @ret int = convert(int, (select session_context(N'CurrentReleaseID')));

	return @ret 
end