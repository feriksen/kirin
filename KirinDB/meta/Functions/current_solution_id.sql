﻿create function meta.current_solution_id()
returns int
begin
	declare @ret int = convert(int, (select session_context(N'CurrentSolutionID')));

	return @ret 
end