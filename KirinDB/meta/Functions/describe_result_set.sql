﻿
CREATE function [meta].[describe_result_set](@source_sql nvarchar(max))
returns table
as
return
select	dyn.* 
from	(
		select	column_name = dyn.[name]
				, column_source_name = dyn.source_column
				, column_source_schema = dyn.source_schema
				, column_source_table = dyn.source_table
				, system_type_id
				, column_max_length = dyn.max_length
				, column_precision = dyn.[precision]
				, column_scale = dyn.scale
				, column_def = dyn.system_type_name
				, is_nullable = dyn.is_nullable
				, is_computed = dyn.is_computed_column
				, is_identity = dyn.is_identity_column
				, dyn.is_part_of_unique_key
				, dyn.column_ordinal
		from	sys.dm_exec_describe_first_result_set(@source_sql, null, 1) dyn
) dyn