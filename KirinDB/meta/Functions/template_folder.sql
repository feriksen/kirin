﻿
CREATE function [meta].[template_foder](@target_type code20)
returns varchar(max)
begin
	declare @area code20 = (select trgt.target_category from meta.target_type trgt where trgt.target_type_code = @target_type)
	declare @template_path varchar(max) = '%APP_PATH%\Templates\%AREA%\Source.%TYPE%.Template.sql'

	set @template_path = replace(@template_path, '%APP_PATH%', meta.application_foder())
	set @template_path = replace(@template_path, '%AREA%', @area)
	set @template_path = replace(@template_path, '%TYPE%', @target_type)

	return @template_path
end