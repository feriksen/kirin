﻿
CREATE proc [meta].[GetTemplateContent]
@target_type code20,
@template_def nvarchar(max) = null output
 as
 set nocount on
begin
	declare @ret int = 0
	declare @exec nvarchar(max) = N'select @template_def = (select bulkcolumn from openrowset(bulk %PATH%,single_blob) x)'
	declare @template_path varchar(max) = meta.template_foder(@target_type)

	set @exec = replace(@exec, '%PATH%', quotename(@template_path,nchar(39)))

	exec sp_executesql @exec, N'@template_def varchar(max) output', @template_def = @template_def output;

	return @ret
end