﻿CREATE proc [meta].[define_kobject]
@kobject_id int = null output,
@source_name str128,
@source_sql dynsql = null output,
@schema_name sysname = null,
@table_name sysname = null
as
begin
	declare @ret int = 0, @err int = 0, @rc int = 0

	declare @type_id int

	set @kobject_id = (
							select	objd.kobject_id
							from	meta.kobject_def objd
							where	objd.kobject_schema_name = @schema_name
							and		objd.kobject_table_name = @table_name
							);
	
	if (@kobject_id is null and @source_sql is not null and @schema_name is null and @table_name is null)
	begin
		set @kobject_id = (
								select	objd.kobject_id
								from	meta.kobject_sql objd
								where	objd.kobject_sql = @source_sql
								); 
	end


	if (@kobject_id is null)
	begin
		set @type_id = (
								select	objt.kobject_type_id
								from	meta.kobject_type objt
										outer apply (
													select	type_code = case 
																		when TABLE_TYPE = 'BASE TABLE' then 'TABLE'
																		else 'VIEW'
																		end
													from	information_schema.tables tb 
													where	tb.TABLE_SCHEMA = @schema_name
													and		tb.TABLE_NAME = @table_name
													) tb
								where	objt.kobject_type_code = case 
																	when @source_sql is not null then 'SQL'
																	else tb.type_code
																	end
								
								);
		
		declare @name str128 = case 
								when @type_id in (100,101) then concat(quotename(@schema_name),'.', quotename(@table_name)) 
								when @type_id = 200 then concat('SQL:', @source_name)
								end

		select @type_id, @name, @schema_name, @table_name, @source_sql

		insert into meta.kobject_def (kobject_type_id, kobject_name, kobject_schema_name, kobject_table_name)
		values (@type_id, @name, @schema_name, @table_name)

		set @kobject_id = SCOPE_IDENTITY();

		if (@source_sql is not null)
		begin
			insert into meta.kobject_sql (kobject_id, kobject_sql)
			values (@kobject_id, @source_sql);
		end
		else 
		begin
			exec meta.get_kobject @kobject_id = @kobject_id, @source_sql = @source_sql output
		end

		exec meta.define_kobject_columns @kobject_id = @kobject_id
	end		

	return @ret
end