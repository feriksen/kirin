﻿

CREATE proc [meta].[define_kobject_columns]
@kobject_id int
as
set nocount on
begin
	declare @ret int = 0, @err int = 0, @rc int = 0

	declare @type_id int
	declare @source_sql dynsql
	declare @schema_name sysname
	declare @table_name sysname

	exec meta.get_kobject @kobject_id = @kobject_id, @type_id = @type_id output, @source_sql = @source_sql output
	
	-- define new column types if any
	insert into meta.column_type(column_def, system_type_id, column_max_length, column_precision, column_scale)
	select	col.column_def
			, col.system_type_id
			, col.column_max_length
			, col.column_precision
			, col.column_scale
	from	meta.describe_result_set(@source_sql) col
	except
	select	colt.column_def
			, colt.system_type_id
			, colt.column_max_length
			, colt.column_precision
			, colt.column_scale
	from	meta.column_type colt
	;

	-- clear out existing object column
	delete from meta.kobject_column where kobject_id = @kobject_id
	
	-- insert new object columns
	insert into meta.kobject_column(kobject_id, parent_kobject_column_id, column_def, column_name, column_source_name, column_source_schema, column_source_table, column_type_id)
	select	@kobject_id
			, parent_object_column_id = coalesce(p_objc.kobject_column_id, -1)
			, col.column_def
			, col.column_name
			, col.column_source_name
			, col.column_source_schema
			, col.column_source_table
			, colt.column_type_id
	from	meta.describe_result_set(@source_sql) col
			outer apply (
						select	colt.column_type_id
						from	meta.column_type colt
						where	colt.system_type_id = col.system_type_id
						and		colt.column_max_length = col.column_max_length
						and		colt.column_precision = col.column_precision
						and		colt.column_scale = col.column_scale
						) colt
			outer apply (
						select	objc.kobject_column_id
						from	meta.kobject_column objc
						where	objc.column_source_name = col.column_source_name
						and		objc.column_source_schema = col.column_source_schema
						and		objc.column_source_table = col.column_source_table
						) p_objc
	-- clear out non-used types
	delete from meta.column_type
	where	column_type_id not in (select objc.column_type_id from meta.kobject_column objc)


	return @ret 
end