﻿
CREATE proc [meta].[define_source] 
@source_id int = null output,
@source_name str128,
@source_sql dynsql = null,
@source_schema sysname = null,
@source_table sysname = null,
@kobject_id int = null
as
set nocount on
begin
	declare @ret int = 0, @err int = 0, @rc int = 0

	if (@kobject_id is null) exec meta.define_kobject @kobject_id = @kobject_id output, @source_name = @source_name, @schema_name = @source_schema, @table_name = @source_table, @source_sql = @source_sql output
	
	insert into meta.source_def(kobject_id, source_name, source_sql)
	values (@kobject_id, @source_name, @source_sql)

	set @source_id = SCOPE_IDENTITY();

	insert into meta.source_column (source_id, kobject_column_id)
	select	@source_id
			, objc.kobject_column_id
	from	meta.kobject_column objc
	where	objc.kobject_id = @kobject_id

	return @ret 
end