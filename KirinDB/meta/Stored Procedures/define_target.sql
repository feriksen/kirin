﻿CREATE proc [meta].[define_target]
@target_id int = null output,
@target_name str128,
@target_type code20,
@target_schema sysname,
@target_table sysname,
@kobject_id int = null
as
set nocount on
begin
	declare @ret int = 0, @err int = 0, @rc int = 0

	declare @temp nvarchar(max)

	if (@kobject_id is null) exec meta.define_kobject @kobject_id = @kobject_id output, @source_name = @target_name, @schema_name = @target_schema, @table_name = @target_table, @source_sql = @temp output 
	
	insert into meta.target_def(kobject_id, target_name, target_type_id)
	select @kobject_id, @target_name, trgt.target_type_id
	from	meta.target_type trgt
	where	trgt.target_type_code = @target_type 

	set @target_id = SCOPE_IDENTITY();

	insert into meta.target_column(target_id, kobject_column_id,is_nullable, target_column_type_id)
	select	@target_id
			, objc.kobject_column_id
			, objc.is_nullable
			, -1
	from	meta.kobject_column objc
	where	objc.kobject_id = @kobject_id

	return @ret
end