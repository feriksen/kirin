﻿
CREATE proc [meta].[get_kobject]
@kobject_id int,
@object_name varchar(256) = null output,
@type_id int = null output,
@source_sql dynsql = null output,
@schema_name sysname = null output,
@table_name sysname = null output
as
set nocount on
begin
	declare @ret int = 0, @err int = 0, @rc int = 0
	
	select	@type_id = objd.kobject_type_id
			, @table_name = objd.kobject_table_name
			, @schema_name = objd.kobject_schema_name
			, @object_name = objd.kobject_name
	from	meta.kobject_def objd
	where	objd.kobject_id = @kobject_id 
	
	if (@type_id = 200)
	begin
		select	@source_sql = objs.kobject_sql
		from	meta.kobject_sql objs
		where	objs.kobject_id = @kobject_id 
	end
	else 
	begin
	--lets give a basic expanded source query for the table/view definition
	declare @column_csv nvarchar(max)
	declare @table_sql nvarchar(max) = replace('select * from <fq_name> where 1=0', '<fq_name>', @object_name)

	;
	with 
	cte_col as
	(
	select	rs.column_name
			, rs.column_ordinal
			, null_comment = case
								when rs.is_nullable = 0 then 'not nullable' 
								else null
								end
			, identity_comment = case
									when rs.is_identity = 1 then 'identity' 
									else null
									end
			, key_comment = case
							when rs.is_part_of_unique_key = 1 then 'part of unique key' 
							else null
							end
			, has_comments = case when rs.is_identity = 1 or rs.is_nullable = 0 or rs.is_part_of_unique_key = 1 then 1 else 0 end
	from	meta.describe_result_set(@table_sql) rs
	),
	cte_src as
	(
	select	column_def = concat(
								case when col.column_ordinal > 1 then ', ' else '' end
								, 'T1.'
								, col.column_name
								, case 
									when has_comments = 1 then concat(
																		char(9)
																		, char(9)
																		, '/* ', col.null_comment, ', ' + col.identity_comment, ', ' + col.key_comment, ' */'
																		) 
									else null 
									end
								, char(10)
								)
			, col.column_ordinal
	from	cte_col col
	)
	select	@column_csv = (select concat(
										char(9)
										, char(9)
										, src.column_def
										)
									from	cte_src src
									order by 
											src.column_ordinal
									for xml path(''))

	
	set @source_sql = replace(concat('SELECT', char(10), '<COLUMN_CSV>FROM', char(9), @object_name, ' T1'), '<COLUMN_CSV>', @column_csv)
	end
	

	return @ret 
end