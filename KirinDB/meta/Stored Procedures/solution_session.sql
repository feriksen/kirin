﻿
create proc meta.solution_session
@solution_id int = null output,
@release_id int = null output
as
set nocount on
begin
	if @solution_id is not null
	begin
		set @release_id = (select release_id from meta.release where solution_id = @solution_id and date_closed is null)
		
		exec sys.sp_set_session_context @key = N'CurrentSolutionID', @value = @solution_id
		exec sys.sp_set_session_context @key = N'CurrentReleaseID', @value = @release_id
	end
	else
	begin
		set @solution_id = meta.current_solution_id();
		set @release_id = meta.current_release_id();
	end
	return 0
end