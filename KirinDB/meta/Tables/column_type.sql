﻿CREATE TABLE [meta].[column_type] (
    [column_type_id]    INT            IDENTITY (1, 1) NOT NULL,
    [column_def]        [dbo].[str128] NULL,
    [system_type_id]    INT            NULL,
    [column_max_length] INT            NULL,
    [column_precision]  INT            NULL,
    [column_scale]      INT            NULL,
    CONSTRAINT [PK_column_type] PRIMARY KEY CLUSTERED ([column_type_id] ASC)
);



