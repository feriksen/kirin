﻿CREATE TABLE [meta].[kirin_config] (
    [config_name]  [sysname]      NOT NULL,
    [config_value] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_kirin_config] PRIMARY KEY CLUSTERED ([config_name] ASC)
);



