﻿CREATE TABLE [meta].[kirin_version] (
    [version_no]    VARCHAR (11)   NULL,
    [version_major] INT            NULL,
    [version_minor] INT            NULL,
    [version_build] INT            NULL,
    [version_name]  NVARCHAR (128) NULL
);

