﻿CREATE TABLE [meta].[kirin_version_history] (
    [version_no]   VARCHAR (11)    NOT NULL,
    [date_created] DATETIME        CONSTRAINT [DF__kirin_ver__date___4356F04A] DEFAULT (getdate()) NULL,
    [created_by]   [dbo].[usrname] NOT NULL,
    CONSTRAINT [PK_kirin_version_history] PRIMARY KEY CLUSTERED ([version_no] ASC)
);



