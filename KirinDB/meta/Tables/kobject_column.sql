﻿CREATE TABLE [meta].[kobject_column] (
    [kobject_column_id]        INT           IDENTITY (1, 1) NOT NULL,
    [parent_kobject_column_id] INT           NOT NULL,
    [kobject_id]               INT           NOT NULL,
    [column_def]               VARCHAR (256) NULL,
    [column_name]              [sysname]     NOT NULL,
    [column_type_id]           INT           NOT NULL,
    [is_nullable]              BIT           NULL,
    [column_source_name]       [sysname]     NOT NULL,
    [column_source_schema]     [sysname]     NOT NULL,
    [column_source_table]      [sysname]     NOT NULL,
    CONSTRAINT [PK_kobject_column] PRIMARY KEY CLUSTERED ([kobject_column_id] ASC),
    CONSTRAINT [FK_kobject_column_column_type] FOREIGN KEY ([column_type_id]) REFERENCES [meta].[column_type] ([column_type_id]),
    CONSTRAINT [FK_kobject_column_kobject_def] FOREIGN KEY ([kobject_id]) REFERENCES [meta].[kobject_def] ([kobject_id])
);



