﻿CREATE TABLE [meta].[kobject_def] (
    [kobject_id]          INT            IDENTITY (1, 1) NOT NULL,
    [release_id] INT            NOT NULL,
    [kobject_type_id]     INT            NOT NULL,
    [kobject_name]        [dbo].[str128] NULL,
    [kobject_schema_name] VARCHAR (128)  NULL,
    [kobject_table_name]  VARCHAR (128)  NULL,
    CONSTRAINT [PK_kobject_def] PRIMARY KEY CLUSTERED ([kobject_id] ASC),
    CONSTRAINT [FK_kobject_def_kobject_type] FOREIGN KEY ([kobject_type_id]) REFERENCES [meta].[kobject_type] ([kobject_type_id]),
    CONSTRAINT [FK_kobject_def_release] FOREIGN KEY ([release_id]) REFERENCES [meta].[release] ([release_id])
);



