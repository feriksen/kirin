﻿CREATE TABLE [meta].[kobject_sql] (
    [kobject_id]          INT            NOT NULL,
    [release_id] INT            NOT NULL,
    [kobject_sql]         [dbo].[dynsql] NULL,
    CONSTRAINT [PK_kobject_sql] PRIMARY KEY CLUSTERED ([kobject_id] ASC),
    CONSTRAINT [FK_kobject_sql_kobject_def] FOREIGN KEY ([kobject_id]) REFERENCES [meta].[kobject_def] ([kobject_id])
);



