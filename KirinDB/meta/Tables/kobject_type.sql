﻿CREATE TABLE [meta].[kobject_type] (
    [kobject_type_id]   INT            NOT NULL,
    [kobject_type_code] [dbo].[code10] NOT NULL,
    [kobject_type_name] [dbo].[str128] NULL,
    CONSTRAINT [PK_kobject_type] PRIMARY KEY CLUSTERED ([kobject_type_id] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_kobject_type_kobject_type_code]
    ON [meta].[kobject_type]([kobject_type_code] ASC);

