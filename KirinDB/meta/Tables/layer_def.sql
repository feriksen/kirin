﻿CREATE TABLE [meta].[layer_def] (
    [layer_id]            INT            IDENTITY (1, 1) NOT NULL,
    [release_id] INT            NOT NULL,
    [layer_code]          [dbo].[code10] NOT NULL,
    [layer_name]          [dbo].[str128] NULL,
    [layer_db_name]       [sysname]      NOT NULL,
    [date_created]        DATETIME       DEFAULT (getdate()) NULL,
    [created_by]          NVARCHAR (256) NULL,
    CONSTRAINT [PK_layer_def] PRIMARY KEY CLUSTERED ([layer_id] ASC),
    CONSTRAINT [FK_layer_def_release] FOREIGN KEY ([release_id]) REFERENCES [meta].[release] ([release_id])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_layer_def_layer_code]
    ON [meta].[layer_def]([layer_code] ASC);

