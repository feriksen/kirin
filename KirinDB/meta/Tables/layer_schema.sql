﻿CREATE TABLE [meta].[layer_schema] (
    [layer_schema_id]     INT             IDENTITY (1, 1) NOT NULL,
    [layer_id]            INT             NOT NULL,
    [release_id] INT             NOT NULL,
    [layer_code]          [dbo].[code10]  NOT NULL,
    [layer_name]          [dbo].[str128]  NULL,
    [layer_db_name]       [sysname]       NOT NULL,
    [is_active]           BIT             DEFAULT ((1)) NULL,
    [date_created]        DATETIME        DEFAULT (getdate()) NULL,
    [created_by]          [dbo].[usrname] NOT NULL,
    [date_modified]       DATETIME        DEFAULT (getdate()) NULL,
    [modified_by]         [dbo].[usrname] NOT NULL,
    CONSTRAINT [PK_layer_schema] PRIMARY KEY CLUSTERED ([layer_schema_id] ASC),
    CONSTRAINT [FK_layer_schema_release] FOREIGN KEY ([release_id]) REFERENCES [meta].[release] ([release_id])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_layer_schema_release_id_layer_id]
    ON [meta].[layer_schema]([release_id] ASC, [layer_id] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_layer_schema_layer_code]
    ON [meta].[layer_schema]([layer_code] ASC);

