﻿CREATE TABLE [meta].[release] (
    [release_id] INT            IDENTITY (1, 1) NOT NULL,
    [solution_id]         INT            NOT NULL,
    [release_code]        [dbo].[code10] NOT NULL,
    [release_name]        [dbo].[str128] NULL,
    [date_created]        DATETIME       DEFAULT (getdate()) NULL,
    [date_closed]         DATETIME       NULL,
    [date_released]       DATETIME       NULL,
    CONSTRAINT [PK_release] PRIMARY KEY CLUSTERED ([release_id] ASC),
    CONSTRAINT [FK_release_solution_def] FOREIGN KEY ([solution_id]) REFERENCES [meta].[solution_def] ([solution_id])
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_release_solution_id_release_code]
    ON [meta].[release]([solution_id] ASC, [release_code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_release_solution_id_date_closed]
    ON [meta].[release]([solution_id] ASC, [date_closed] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_release_release_code]
    ON [meta].[release]([release_code] ASC);

