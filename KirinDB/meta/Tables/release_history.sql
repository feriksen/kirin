﻿CREATE TABLE [meta].[release_history] (
    [release_history_id] INT             IDENTITY (1, 1) NOT NULL,
    [release_id]         INT             NOT NULL,
    [history_message]             NVARCHAR (MAX)  NULL,
    [date_created]                DATETIME        DEFAULT (getdate()) NULL,
    [created_by]                  [dbo].[usrname] NOT NULL,
    CONSTRAINT [PK_release_history] PRIMARY KEY CLUSTERED ([release_history_id] ASC),
    CONSTRAINT [FK_release_history_release] FOREIGN KEY ([release_history_id]) REFERENCES [meta].[release] ([release_id])
);



