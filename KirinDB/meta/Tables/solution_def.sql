﻿CREATE TABLE [meta].[solution_def] (
    [solution_id]   INT             IDENTITY (1, 1) NOT NULL,
    [solution_code] [dbo].[code10]  NOT NULL,
    [solution_name] [dbo].[str128]  NULL,
    [date_created]  DATETIME        DEFAULT (getdate()) NULL,
    [created_by]    [dbo].[usrname] NOT NULL,
    CONSTRAINT [PK_solution_def] PRIMARY KEY CLUSTERED ([solution_id] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_solution_solution_code]
    ON [meta].[solution_def]([solution_code] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_solution_def_solution_code]
    ON [meta].[solution_def]([solution_code] ASC);

