﻿CREATE TABLE [meta].[source_column] (
    [source_column_id]  INT            IDENTITY (1, 1) NOT NULL,
    [source_id]         INT            NOT NULL,
    [kobject_column_id] INT            NOT NULL,
    [column_alias]      NVARCHAR (128) NULL,
    CONSTRAINT [PK_source_column] PRIMARY KEY CLUSTERED ([source_column_id] ASC),
    CONSTRAINT [FK_source_column_kobject_column] FOREIGN KEY ([kobject_column_id]) REFERENCES [meta].[kobject_column] ([kobject_column_id]),
    CONSTRAINT [FK_source_column_source_def] FOREIGN KEY ([source_id]) REFERENCES [meta].[source_def] ([source_id])
);



