﻿CREATE TABLE [meta].[source_def] (
    [source_id]           INT            IDENTITY (1, 1) NOT NULL,
    [release_id] INT            NOT NULL,
    [kobject_id]          INT            NOT NULL,
    [source_name]         [dbo].[str128] NULL,
    [source_sql]          [dbo].[dynsql] NULL,
    CONSTRAINT [PK_source_def] PRIMARY KEY CLUSTERED ([source_id] ASC),
    CONSTRAINT [FK_source_def_release] FOREIGN KEY ([release_id]) REFERENCES [meta].[release] ([release_id]),
    CONSTRAINT [FK_source_def_release1] FOREIGN KEY ([release_id]) REFERENCES [meta].[release] ([release_id])
);



