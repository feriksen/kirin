﻿CREATE TABLE [meta].[target_column] (
    [target_column_id]      INT IDENTITY (1, 1) NOT NULL,
    [target_id]             INT NOT NULL,
    [kobject_column_id]     INT NOT NULL,
    [target_column_type_id] INT NOT NULL,
    [is_nullable]           BIT NULL,
    CONSTRAINT [PK_target_column] PRIMARY KEY CLUSTERED ([target_column_id] ASC),
    CONSTRAINT [FK_target_column_kobject_column] FOREIGN KEY ([kobject_column_id]) REFERENCES [meta].[kobject_column] ([kobject_column_id]),
    CONSTRAINT [FK_target_column_target_column_type] FOREIGN KEY ([target_column_type_id]) REFERENCES [meta].[target_column_type] ([target_column_type_id]),
    CONSTRAINT [FK_target_column_target_def] FOREIGN KEY ([target_id]) REFERENCES [meta].[target_def] ([target_id])
);



