﻿CREATE TABLE [meta].[target_column_type] (
    [target_column_type_id]          INT            IDENTITY (1, 1) NOT NULL,
    [target_type_id]                 INT            NULL,
    [target_column_type_code]        [dbo].[code20] NOT NULL,
    [target_column_type_description] [dbo].[str128] NULL,
    CONSTRAINT [PK_target_column_type] PRIMARY KEY CLUSTERED ([target_column_type_id] ASC),
    CONSTRAINT [FK_target_column_type_target_type] FOREIGN KEY ([target_type_id]) REFERENCES [meta].[target_type] ([target_type_id])
);



