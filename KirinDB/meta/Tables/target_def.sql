﻿CREATE TABLE [meta].[target_def] (
    [target_id]           INT            IDENTITY (1, 1) NOT NULL,
    [release_id] INT            NOT NULL,
    [kobject_id]          INT            NOT NULL,
    [target_type_id]      INT            NULL,
    [target_name]         [dbo].[str128] NULL,
    CONSTRAINT [PK_target_def] PRIMARY KEY CLUSTERED ([target_id] ASC)
);



