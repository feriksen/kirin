﻿CREATE TABLE [meta].[target_type] (
    [target_type_id]          INT            NOT NULL,
    [target_type_code]        [dbo].[code20] NOT NULL,
    [target_type_description] [dbo].[str128] NULL,
    [target_category]         [dbo].[code20] NOT NULL,
    [target_sub_category]     [dbo].[code20] NOT NULL,
    [is_historized]           BIT            DEFAULT ((0)) NULL,
    [is_snapshot]             BIT            DEFAULT ((0)) NULL,
    [is_transactional]        BIT            DEFAULT ((0)) NULL,
    CONSTRAINT [PK_target_type] PRIMARY KEY CLUSTERED ([target_type_id] ASC)
);






GO
CREATE UNIQUE NONCLUSTERED INDEX [ix_target_type_target_type_code]
    ON [meta].[target_type]([target_type_code] ASC);

