﻿/*
Post-Deployment Script 							
--------------------------------------------------------------------------------------
 Initial seeding of Kirin specific values			
--------------------------------------------------------------------------------------
*/

USE [metadb]
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (101, N'Extract', N'Landing Area: Extract layer', N'Stage', N'Extract', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (102, N'Archive_T2', N'Landing Area: Condensed Archive layer (PSA)', N'Stage', N'Archive', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (103, N'Archive_TS', N'Landing Area: Snapshot Archive layer (PSA)', N'Stage', N'Archive', 0, 1, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (201, N'Temp', N'Staging Area: intermediate volatile', N'Stage', N'Temp', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (202, N'Stage', N'Staging Area: intermediate non-volatile (within batch)', N'Stage', N'Temp', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (301, N'Hub', N'Vault: Hub table', N'Vault', N'Hub', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (302, N'HubSat_T1', N'Vault: Hub Satellite table Type 1 (transactional)', N'Vault', N'Sat', 0, 0, 1)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (303, N'HubSat_T2', N'Vault: Hub Satellite table Type 2 (Historized', N'Vault', N'Sat', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (304, N'HubSat_ET', N'Vault: Hub Satellite Effectivity', N'Vault', N'Sat', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (311, N'Link', N'Vault: Link table', N'Vault', N'Link', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (312, N'LinkSat_T1', N'Vault: Link Satellite table Type 1 (transactional)', N'Vault', N'Sat', 0, 0, 1)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (313, N'LinkSat_T2', N'Vault: Link Satellite table Type 2 (Historized', N'Vault', N'Sat', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (314, N'LinkSat_ET', N'Vault: Link Satellite Effectivity', N'Vault', N'Sat', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (321, N'PIT', N'Vault: Point In Time table', N'Vault', N'', 1, 1, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (322, N'Bridge_T1', N'Vault: Bridge table type 1 (Non-historical)', N'Vault', N'', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (323, N'Bridge_T2', N'Vault: Bridge table type 2 (Snapshot based)', N'Vault', N'', 0, 1, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (324, N'Bridge_T3', N'Vault: Bridge table type 3 (Historized)', N'Vault', N'', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (901, N'Dimension_T0', N'Data Mart: Dimension SCD:Type 0', N'DM', N'Dim', 0, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (902, N'Dimension_T1', N'Data Mart: Dimension SCD:Type 1', N'DM', N'Dim', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (903, N'Dimension_T2', N'Data Mart: Dimension SCD: Type 2', N'DM', N'Dim', 1, 0, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (911, N'Fact_T0', N'Data Mart: Fact (transactions)', N'DM', N'Fact', 0, 0, 1)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (912, N'Fact_TS', N'Data Mart: Fact (snapshot based)', N'DM', N'Fact', 0, 1, 0)
GO
INSERT [meta].[target_type] ([target_type_id], [target_type_code], [target_type_description], [target_category], [target_sub_category], [is_historized], [is_snapshot], [is_transactional]) VALUES (913, N'Fact_T2', N'Data Mart: Fact (temporal)', N'DM', N'Fact', 1, 0, 0)
GO
SET IDENTITY_INSERT [meta].[target_column_type] ON 
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (1, 101, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (2, 101, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (3, 101, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (4, 101, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (5, 102, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (6, 102, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (7, 102, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (8, 102, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (9, 102, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (10, 102, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (11, 102, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (12, 102, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (13, 103, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (14, 103, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (15, 103, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (16, 103, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (17, 201, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (18, 201, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (19, 201, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (20, 201, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (21, 202, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (22, 202, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (23, 202, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (24, 202, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (25, 301, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (26, 301, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (27, 301, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (28, 301, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (29, 301, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (30, 301, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (31, 301, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (32, 301, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (33, 302, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (34, 302, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (35, 302, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (36, 302, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (37, 302, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (38, 302, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (39, 302, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (40, 302, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (41, 303, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (42, 303, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (43, 303, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (44, 303, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (45, 303, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (46, 303, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (47, 303, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (48, 303, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (49, 303, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (50, 303, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (51, 303, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (52, 303, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (53, 304, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (54, 304, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (55, 304, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (56, 304, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (57, 304, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (58, 304, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (59, 304, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (60, 304, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (61, 304, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (62, 304, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (63, 304, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (64, 304, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (65, 311, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (66, 311, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (67, 311, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (68, 311, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (69, 311, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (70, 311, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (71, 311, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (72, 311, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (73, 312, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (74, 312, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (75, 312, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (76, 312, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (77, 312, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (78, 312, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (79, 312, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (80, 312, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (81, 313, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (82, 313, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (83, 313, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (84, 313, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (85, 313, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (86, 313, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (87, 313, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (88, 313, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (89, 313, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (90, 313, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (91, 313, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (92, 313, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (93, 314, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (94, 314, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (95, 314, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (96, 314, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (97, 314, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (98, 314, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (99, 314, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (100, 314, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (101, 314, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (102, 314, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (103, 314, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (104, 314, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (105, 321, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (106, 321, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (107, 321, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (108, 321, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (109, 321, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (110, 321, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (111, 321, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (112, 321, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (113, 321, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (114, 321, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (115, 321, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (116, 321, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (117, 322, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (118, 322, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (119, 322, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (120, 322, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (121, 322, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (122, 322, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (123, 322, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (124, 322, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (125, 323, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (126, 323, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (127, 323, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (128, 323, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (129, 323, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (130, 323, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (131, 323, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (132, 323, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (133, 324, N'Meta_BK', N'Entity Business Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (134, 324, N'Meta_DTS', N'Load Audit Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (135, 324, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (136, 324, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (137, 324, N'Meta_EndDTS', N'Load Audit End Date')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (138, 324, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (139, 324, N'Meta_SRC', N'Load Audit Source')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (140, 324, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (141, 324, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (142, 324, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (143, 324, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (144, 324, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (145, 901, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (146, 901, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (147, 901, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (148, 901, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (149, 902, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (150, 902, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (151, 902, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (152, 902, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (153, 902, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (154, 902, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (155, 902, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (156, 902, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (157, 903, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (158, 903, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (159, 903, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (160, 903, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (161, 903, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (162, 903, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (163, 903, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (164, 903, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (165, 911, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (166, 911, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (167, 911, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (168, 911, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (169, 912, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (170, 912, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (171, 912, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (172, 912, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (173, 913, N'Meta_EffDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (174, 913, N'Meta_EffEndDTS', N'Record Effective Timestamp')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (175, 913, N'Meta_LoadTK', N'Load Audit Key')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (176, 913, N'Meta_Standard', N'non-specific usage defined')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (177, 913, N'Meta_T0', N'Column Type 0')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (178, 913, N'Meta_T1', N'Column Type 1')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (179, 913, N'Meta_T2', N'Column Type 2')
GO
INSERT [meta].[target_column_type] ([target_column_type_id], [target_type_id], [target_column_type_code], [target_column_type_description]) VALUES (180, 913, N'Meta_UpdateTK', N'Load Audit Key for record update')
GO
SET IDENTITY_INSERT [meta].[target_column_type] OFF
GO
