%SOURCE_VARIABLES%
;
with
cte_source as
(
%SOURCE_SQL%
)
select	%BUSINESS_KEY_CSV%
		%META_COLUMN_CSV%
from	cte_source src
%SOURCE_WHERE_FILTER%